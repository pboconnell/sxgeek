const dbconfig = require('../dbconfig.json');
const mysql = require('mysql');
const connection = mysql.createConnection(dbconfig);
const sqlstr = require('sqlstring');

const TILE_WIDTH = 3600;

exports.getPoint = function(lat, lon, fields, cb) {
	var coords = coord_to_index(lat, lon);
	var field_str = fields.join(', ')
	console.log(field_str);
	var sql = sqlstr.format('select ?? from location_data where lat=? and lon=?', [fields, coords['lati'], coords['loni']]);
	console.log(sql);
	connection.query(sql, (err, results, fields) => {
		if(err) {
			console.log('sql error');
			cb(err, null);
			return;
		}
		if(results.length === 0) {
			console.log('no results');
			cb(Error('no results found'), null);
		}
		else {
			console.log(results[0].height);
			cb(null, results[0]);
		}
	});
}

exports.getBbox = function(bbox, cols, cb) {
	var sw_coords = coord_to_index(bbox.south, bbox.west);
	var ne_coords = coord_to_index(bbox.north, bbox.east);

	var expected_size = (Math.abs(ne_coords.lati - sw_coords.lati) + 1) * (Math.abs(ne_coords.loni - sw_coords.loni) + 1);

	console.log(expected_size);

	var sql = sqlstr.format('select ?? from location_data where lat between ? and ? and (lon between ? and ?)', [cols, ne_coords.lati, sw_coords.lati, sw_coords.loni, ne_coords.loni]);
	
	console.log(sql);
	
	connection.query(sql, (err, results, fields) => {
		if(err) {
			console.log('sql error: ' + err);
			cb(err, null);
		}
		else if(results.length !== expected_size) {
			console.log('part of bbox out of bounds');
			cb(Error('bbox out of bounds'), null);
		}
		else {
			console.log(results[0]);
			var resp = {}
			for(i=0; i<cols.length; i++) {
				resp[cols[i]] = {}
				resp[cols[i]]['ptsPerRow'] = Math.abs(ne_coords.loni - sw_coords.loni) + 1
				resp[cols[i]]['ptsPerDegree'] = TILE_WIDTH;
				var data = results.map(a => a[cols[i]]);
				var dataStr = Buffer.from(data).toString('base64');
				var parsedData = new Buffer(dataStr, 'base64');
				console.log('parsedData: ' + parsedData);
				console.log(cols[i] + ': ' + data[0]);
				console.log(typeof data[0]);
				console.log(dataStr.length);
				console.log(Buffer.from([data[0]]).toString('base64'));
				console.log(dataStr.substring(0,5));
			}

		}
	});
}

exports.getTile = function(type, x, y, z, cb) {
	var sql = sqlstr.format('select data from tiles where type=? and x=? and y=? and z=?', [type, x, y, z]);
	connection.query(sql, (err, results, fields) => {
		if(err) {
			console.log('sql error: ' + err);
			cb(err, null);
		}
		else if(results.length === 0) {
			console.log('no results');
			cb(Error('no results found'), null);
		}
		else {
			cb(null, results[0].data);
		}
	});
}

function _arrayBufferToBase64( buffer ) {
	var binary = '';
	var bytes = new Uint8Array( buffer );
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode( bytes[ i ] );
	}
	return window.btoa( binary );
}

coord_to_index = function(lat, lon) {
	var results = {};
	var start_lon = Math.floor(lon) * TILE_WIDTH;
	var offest_lon = Math.round((lon - Math.floor(lon)) * TILE_WIDTH);
	results['loni'] = start_lon + offest_lon;

	var start_lat = Math.floor(lat) * TILE_WIDTH;
	var offest_lat = Math.round((lat - Math.floor(lat)) * TILE_WIDTH);
	results['lati'] = start_lat - offest_lat;

	return results;
}