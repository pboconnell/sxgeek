var po = org.polymaps;

var alt = 0;

var map = po.map()
	.container(document.body.appendChild(po.svg("svg")))
	.center({lat: 46.8540115, lon: -121.768827})
	.zoom(13)
	.add(po.image()
		.url("http://tileserver.trimbleoutdoors.com/SecureTile/TileHandler.ashx?mapType=Topo&x={X}&y={Y}&z={Z}"))
	.add(po.image()
		// .url("http://mt.google.com/vt/lyrs=m&x={X}&y={Y}&z={Z}"))
		.url("http://ec2-34-214-201-20.us-west-2.compute.amazonaws.com:3000/tile/0/{X}/{Y}/{Z}"))
	.add(po.interact());

//-121.818094,46.827129,-121.71956,46.880894
// $.getJSON("getSlope?bbox=-121.831562,46.818373,-121.70522,46.88658", function(data){
// 	console.log('min: ' + data['min'] + ' diff: ' + data['diff']);
// 	drawSlopes(data);
// 	// drawHeights(data);
// });

function drawSlopes(json) {
	var datahex = base64toHEX(json['base64String']).split("");

	var lat = json['sw']['lat'];
	var lon = json['sw']['lon'];
	var rowStart = lon;
	var inc = 1/json['ptsPerDegree'];
	var cols = json['ptsPerRow'];
	var min = json['min'];
	var diff = json['diff'];

	console.log("inc: " + inc + " cols: " + cols);
	console.log(datahex.length);

	var features = [];

	while(datahex.length > 0) {
		for(var i=0; i<cols; i++) {
			var hex = datahex.splice(0,2).join("");
			features.push(makePoly(hex, lat, lon, inc, min, diff));
			lon += inc;
		}
		lat += inc;
		lon = rowStart;
	}
	console.log('features.length: ' + features.length);
	addFeatures(features);
	// slowDraw(features);
}

function drawHeights(json) {
	var datahex = base64toHEX(json['base64String']).split("");

	var lat = json['sw']['lat'];
	var lon = json['sw']['lon'];
	var rowStart = lon;
	var inc = 1/json['ptsPerDegree'];
	var cols = json['ptsPerRow'];
	var min = json['min'];
	var diff = json['diff'];

	console.log("inc: " + inc + " cols: " + cols);
	console.log(datahex.length);

	var features = [];

	while(datahex.length > 0) {
		for(var i=0; i<cols; i++) {
			var hex = datahex.splice(0,4).join("");
			features.push(makePoly(hex, lat, lon, inc, min, diff));
			lon += inc;
		}
		lat += inc;
		lon = rowStart;
	}
	console.log('features.length: ' + features.length);
	addFeatures(features);
	// slowDraw(features);
}

function slowDraw(features) {
	map.add(po.geoJson()
		.features([features.shift()])
		.on("load", load));
	if(features.length > 0) {
		setTimeout(function() {
			slowDraw(features);
		}, 1);
	}
}

function makePoly(hex, lat, lon, inc, min, diff) {
	alt = Math.floor(Math.random() * 2);
	var testcolor;
	if(alt == 0) {
		testcolor = "#f0000f";
	}
	else {
		testcolor = "#0f00f0";
	}
	var meters = parseInt(hex, 16);
	// var percentage = ((meters - min) / diff);
	// var colorStr = "#" + Math.round((255 * percentage)).toString(16) + "0000";
	//var colorStr = "#" + Math.round((255 * percentage)).toString(16) + "00" + Math.round((255 * (1 - percentage))).toString(16);
	var colorStr;
	if(meters < 10)
		colorStr = "#00ff00"
	else if(meters >= 10 && meters < 45)
		colorStr = "#ff0000"
	else
		colorStr = "#0000ff"

	var geoJson = {
		"type": "Feature",
		"properties": {
			"stroke-width": 0,
			"fill": colorStr,
			"fill-opacity": .5
		},
		"geometry": {
		"coordinates": [[[
			[lon, lat],
			[lon, lat + inc],
			[lon + inc, lat + inc],
			[lon + inc, lat],
			[lon, lat],
		]]],
		"type": "MultiPolygon"
		}
	};

	return geoJson;
}

function addFeatures(features) {
	map.add(po.geoJson()
		.features(features)
		.on("load", load));
}

function base64toHEX(base64) {
	var raw = atob(base64);
	var hex = '';
	for (var i = 0; i < raw.length; i++) {
		var byte = raw.charCodeAt(i).toString(16);
		hex += (byte.length==2?byte:'0'+byte);
	}
	return hex.toLowerCase();
}

// map.add(po.geoJson()
// //-121.818094	46.827129	-121.71956	46.880894
// 	.features([
// 	{
// 		"type": "Feature",
// 		"properties": {
// 			"stroke-width": 0,
// 			"fill": colorStr,
// 			"fill-opacity": .7
// 		},
// 		"geometry": {
// 		"coordinates": [[[
// 			[-121.818094, 46.827129],
// 			[-121.818094, 46.880894],
// 			[-121.71956, 46.880894],
// 			[-121.71956, 46.827129],
// 			[-121.818094, 46.827129],
// 		]]],
// 		"type": "MultiPolygon"
// 		}
// 	}])
// 	.on("load", load));

function load(e) {
	console.log('in load');
	console.log(e);
	for(var i=0; i<e.features.length; i++) {
		var c = n$(e.features[i].element),
			g = c.parent().add("svg:g", c);
		g.add(c
			.attr("fill", e.features[i].data.properties.fill)
			.attr("fill-opacity", e.features[i].data.properties['fill-opacity']));
	}
}
