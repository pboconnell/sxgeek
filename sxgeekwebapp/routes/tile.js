const express = require('express');
const router = express.Router();

const db = require('../libs/db_controller');

router.get('/:type/:x/:y/:z', function(req, res) {
	db.getTile(req.params.type, req.params.x, req.params.y, req.params.z, (err, results) => {
		if(err) {
			console.log('db error: ' + err);
			res.sendStatus(404);
		}
		else {
			res.setHeader('content-type', 'image/png');
			res.end(results);
		}
	});
});

module.exports = router;
