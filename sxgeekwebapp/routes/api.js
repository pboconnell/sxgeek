const express = require('express');
const router = express.Router();

const db = require('../libs/db_controller');

router.get('/point', function(req, res, next) {
	fields = (req.query.fields).split(',');
	var lat = req.query.lat;
	var lon = req.query.lon;
	console.log(fields);
	console.log(lat);
	db.getPoint(lat, lon, fields, (err, results) => {
		if(err) {
			console.log('db error: ' + err);
			res.sendStatus(404);
		}
		else {
			res.json(results);
		}
	});
});

router.get('/bbox', function(req, res, next) {
	res.end();
});

module.exports = router;
