const express = require('express')
const app = express()
const port = 3000
const dbconfig = require('./dbconfig.json')

var mysql = require('mysql')
var connection = mysql.createConnection(dbconfig)
var sqlstr = require('sqlstring')

app.set('view engine', 'pug')

app.use(express.static('static'))

app.get('/tile/:type/:x/:y/:z', function(req, res) {
	console.log(req.params.x)
	sql = sqlstr.format('select data from tiles where type=? and x=? and y=? and z=?', [req.params.type, req.params.x, req.params.y, req.params.z])
	connection.query(sql, function(err, results, fields) {
		if(err) {
			res.end('err')
			return
		}
		if(results.length == 0) {
			res.end('no results')
			return
		}
		res.setHeader('content-type', 'image/png')
		res.end(results[0].data)
	})
})

app.get('/', function(req, res) {
	res.render('map');
})

app.listen(port, () => console.log('Example app listening on port ${port}!'))
