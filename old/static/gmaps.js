//4384
//262

// var heightMin = 262;
// var heightDiff = 4122;

var map;

function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(46.5, -121.5),
		zoom: 10
	};//-121.818094	46.827129	-121.71956	46.880894
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	$.getJSON("getRect?bbox=-121.818094,46.827129,-121.71956,46.880894", function(data){
		drawHeights(data);
	});
}

function drawHeights(json) {
	var datahex = base64toHEX(json['base64String']).split("");

	var lat = json['sw']['lat'];
	var lon = json['sw']['lon'];
	var rowStart = lon;
	var inc = 1/json['ptsPerDegree'];
	var cols = json['ptsPerRow'];
	var min = json['min'];
	var diff = json['diff'];

	console.log("inc: " + inc + " cols: " + cols);
	console.log(datahex.length);

	while(datahex.length > 0) {
		for(var i=0; i<cols; i++) {
			var hex = datahex.splice(0,4).join("");
			drawPoly(hex, lat, lon, inc, min, diff);
			lon += inc;
		}
		lat += inc;
		lon = rowStart;
	}
}

function drawPoly(hex, lat, lon, inc, min, diff) {
	var meters = parseInt(hex, 16);
	var percentage = ((meters - min) / diff);
	console.log(min);
	console.log(diff);
	console.log("percentage: " + percentage);
	var colorStr = "#" + Math.round((255 * percentage)).toString(16) + "00" + Math.round((255 * (1 - percentage))).toString(16);
	console.log(colorStr);
	var coords = [
		{lat: lat, lng: lon},
		{lat: (lat+inc), lng: lon},
		{lat: (lat+inc), lng: (lon+inc)},
		{lat: lat, lng: (lon+inc)},
		{lat: lat, lng: lon}
	];
	var poly = new google.maps.Polygon({
		paths: coords,
		strokeWeight: 0,
		fillColor: colorStr,
		fillOpacity: .5
	});
	poly.setMap(map);
}

function base64toHEX(base64) {
	var raw = atob(base64);
	var hex = '';
	for (var i = 0; i < raw.length; i++) {
		var byte = raw.charCodeAt(i).toString(16);
		hex += (byte.length==2?byte:'0'+byte);
	}
	return hex.toLowerCase();
}

//http://localhost:5000/getRect?bbox=-121.8,46.8,-121.7,46.9